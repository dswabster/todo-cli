# todo
## command line todo management

There are a bunch of planned features. However, at the moment, all todo does is
allow you to append a line of text to a todo.txt file in the local directory.

Usage:

	todo help - prints usage options
	todo -c "hey, its a task!" 
 	todo -r - reads todo.txt file
     todo -i prints number of items in list
   	todo -l [x] - prints item on line x



Contributons by Danny Swaby
Copyright 2012 Abtin Forouzandeh

This file is part of todo.

todo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

todo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with todo.  If not, see <http://www.gnu.org/licenses/>.

