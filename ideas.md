# todo
## Some ideas for what to do

### Features

* Human readable todo file - or maybe sqlite db?
* Manage todo list from command line
* Time tracking
* Checksum to verify file integrity
* Task archiving
* Prioritize tasks
* Project management
* Location management
* Generate timesheets
* Recurring tasks
* Suggest next task
* Support task management system - gtd?
* Sync server
* Display calendar
* Diary of daily activity
* Journaling of all actions
* Subtasks/dependencies
* Generate js/html version... or maybe run internal web server
* Generate human readable file from sqlite?
* Task notes
* Due dates
* Search
* Checklists - create new set of tasks from checklist template
* Associate URL with project?
* Task ordering
* If .todo exists in current dir, use that instead

### Usage

* Add a task
	todo create this is a great task
	> task 1 created
* List tasks
	todo read
	> 1\tcreated_date\tthis is a great task
* Alter task
	todo update 1 this *was* a great task
	todo read
	> 1\tcreated_date\tmodifieddate\t this *was* a great task
* Delete task
	todo delete 1
* Mark task completed
	todo check 1
	>1\tcompleted_date\t...
* Archive task
	todo archive 1
* Start work on task
	todo start 1
* Stop work on task
	todo stop 1
* Create subtask
	todo subtask 1 create this is a subtask to task 1
	> task 2 created
* Make subtask
	todo subtask 1 update 2
* Move subtask up task tree
	todo subtask climb update 2
* Add owner to task
	todo owner add bob 2

### Configuring ~/.todorc

* Default file: ~/.todo
* Dfault journal: ~/todo.hist
* Specify aliases to alternate files
* Specify colors for priorities
* Specify colors for projects and tags
* Auto-commit to git
* Populate user info from git if available



Copyright 2012 Abtin Forouzandeh

This file is part of todo.

todo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

todo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with todo.  If not, see <http://www.gnu.org/licenses/>.

