// Contributions by Danny Swaby
// Copyright 2012 Abtin Forouzandeh
//
// This file is part of todo.
//
// todo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// todo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with todo.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _main_hpp_included_
#define _main_hpp_included_
const std::string kActionUsage = "help";//Usage option
const std::string kActionRead = "-r";//option used for reading list data
const std::string kActionCreate = "-c";// option used for creating list event
const std::string kActionLines = "-i"; //option to print number of items in file
const std::string kActionReadLine = "-l";//option to read line

#endif
