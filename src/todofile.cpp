// Contributions made by Danny Swaby
// Copyright 2012 Abtin Forouzandeh
//
// This file is part of todo.
//
// todo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// todo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with todo.  If not, see <http://www.gnu.org/licenses/>.

#include "todofile.hpp"
#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <sstream>
#include <iomanip>




namespace todo {
    
    TodoFile::TodoFile(std::string file_name) {
        
        file_name_ = file_name;
        
        std::fstream file(file_name.c_str(), std::fstream::in);
        
        std::string line;
        
        while(getline(file, line))
            
			items_.push_back(line);
    }
    
    TodoFile::~TodoFile() {
    }
    
    void TodoFile::Write() {
        
		std::string temp_file = file_name_ + "~";
        
		std::fstream file(temp_file.c_str(), std::fstream::out);
        
		while(!items_.empty()){
            
			file<<items_.front()<<std::endl;
            
			items_.pop_front();
		}
		remove(file_name_.c_str());
        
		rename(temp_file.c_str(), file_name_.c_str());
	}
    
    
    //creates formatted string with item number and date to prepend at beginning of 
	int TodoFile::AddItem(std::string item) {
        
        std::string lineId = MakeIdentifier();
        
        std::string arTime = TimeString();
        
        //remove /n charcter and prepend
        arTime.erase(std::remove(arTime.begin(), arTime.end(), '\n'), arTime.end());
        
        std::string nItem ='<' + lineId +'>' + '<' + arTime + '>' + ' ' + item;
        
        items_.push_back(nItem);
        
		return 0;
	}
    
    
    //print number of lines to cl
    int TodoFile::PrintCount(){
        
        int ln;
        
        std::ifstream inf(file_name_.c_str());
        
        ln = CountLines();
        
        std::cout << "There are " << ln << " items in your todo file" << std::endl;
        
        return 0;
    }
    
    //takes file name and line number argument provided and prints line
    void TodoFile::ReadLine(std::string lineNum){
        
        std::ifstream input(file_name_.c_str());
        
        std::string s;
        
        int value = atoi(lineNum.c_str());
        
        for(int i = 1; i < value; ++i)
            
            std::getline(input, s);
        
        std::getline(input,s);
        
        std::cout << s << std::endl;
    }
    
    //prints content of file
    int TodoFile::PrintList(){
        
        std::ifstream inf(file_name_.c_str());
        
        if (!inf)
        {
            // Print an error and exit
            
            std::cerr << "Could not open " << file_name_ << std::endl;
            
            return 1;
        }
        while (inf)
        {
            // read file 
            std::string strInput;
            
            getline(inf, strInput);
            
            std::cout << strInput << std::endl;
        }
        return 0;
    }
    
    
    //************************* Helper Functions **********************************
    
    
    //counts the current number of lines in todo
    int TodoFile::CountLines(){
        
        unsigned int number_of_lines = 0;
        
        FILE *infile = fopen(file_name_.c_str(), "r");
        
        int ch;
        
        while (EOF != (ch=getc(infile)))
            
            if ('\n' == ch)
                
                ++number_of_lines;
        
        return number_of_lines;
    }
    
    
    //returns string of number for next line
    std::string TodoFile::MakeIdentifier(){
        
        int lineNumber = CountLines();
        
        
        lineNumber++;
        
        std::string OutString = convertInt(int (lineNumber));
        
        return OutString;
        
    }
    
    
    std::string TodoFile::convertInt(int number)
    {
        
        std::stringstream ss;
        
        ss << number;
        
        return ss.str();    }
    
    //return time string w/o newline
    
    std::string TodoFile::TimeString(){
        
        //create string w/current time and format for output 
        time_t rawtime;
        
        time ( &rawtime );
        
        std::string arTime = ctime (&rawtime);
        
        //remove /n charcter and prepend
        arTime.erase(std::remove(arTime.begin(), arTime.end(), '\n'), arTime.end());
        
        return arTime;
    }
    
}
