// Contributions by Danny Swaby
// Copyright 2012 Abtin Forouzandeh
//
// This file is part of todo.
//
// todo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// todo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with todo.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <cstdlib>
#include <time.h>
#include "main.hpp"
#include "todofile.hpp"


int main(int argc, char *argv[], char *envp[]) {
    
    if(argc < 2) {
        std::cout << "Invalid Number of Arguments, help for usage" << std::endl;
        return 1;
    }
    
    std::string todoFile = "todo.txt";
    
    std::string action = argv[ 1 ];
    
	todo::TodoFile t(todoFile.c_str());
    
    if (action == kActionUsage) 
        
        std::cout << "Usage: /\n-r read todo file /\n-c create an event /\n-i print number of items /\n-l [x] print item x" << std::endl;
    
	if(action == kActionCreate){
        
        t.AddItem(argv[2]);
        
        std::cout << "Todo Item added" << std::endl;
    }
    t.Write(); //will create text file if none available so not to cause errors w/following operations
    
    if(action == kActionRead)
        
		t.PrintList();
    
    if(action == kActionLines)
        
        t.PrintCount();
    
    if (action == kActionReadLine) {
        
        t.ReadLine(argv[2]);
    }
    else
		return 1;
    
	return 0;
}

