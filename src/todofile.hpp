// Extended by Danny Swaby
// Copyright 2012 Abtin Forouzandeh
//
// This file is part of todo.
//
// todo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// todo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with todo.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _todofile_hpp_included_
#define _todofile_hpp_included_

#include <iostream>
#include <list>
#include <string>
#include <fstream>

namespace todo {
    
	class TodoFile {
    public:
		TodoFile(std::string file_name);
		~TodoFile();
		void Write();
		int AddItem(std::string item);
        int PrintList();
        int NewFile(std::string file_name); 
        int PrintCount();
        std::string TimeString();
        void ReadLine(std::string LineNumber);
        
        
        
    private:
		std::string file_name_;
		std::list<std::string> items_;
        std::string convertInt(int number);
        std::string MakeIdentifier();
        int CountLines();
        
        
	};
    
}

#endif
